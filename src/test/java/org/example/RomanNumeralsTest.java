package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

    public class RomanNumeralsTest
    {
        RomanNumerals romanNumerals = new RomanNumerals();

        @Test
        public void parseIReturn1() throws Exception {
            assertEquals(romanNumerals.parse("I"),1);
        }
        @Test
        public void parseIIReturn2() throws Exception {
            assertEquals(romanNumerals.parse("II"),2);
        }
        @Test
        public void parseIIIReturn3() throws Exception {
            assertEquals(romanNumerals.parse("III"),3);
        }
        @Test
        public void parseIVReturn4() throws Exception {
            assertEquals(romanNumerals.parse("IV"),4);
        }
        @Test
        public void parseVReturn5() throws Exception {
            assertEquals(romanNumerals.parse("V"),5);
        }
        @Test
        public void parseVIReturn6() throws Exception {
            assertEquals(romanNumerals.parse("VI"),6);
        }
        @Test
        public void parseVIIReturn7() throws Exception {
            assertEquals(romanNumerals.parse("VII"),7);
        }
        @Test
        public void parseVIIIReturn8() throws Exception {
            assertEquals(romanNumerals.parse("VIII"),8);
        }
        @Test
        public void parseIXReturn9() throws Exception {
            assertEquals(romanNumerals.parse("IX"),9);
        }
        @Test
        public void parseXReturn10() throws Exception {
            assertEquals(romanNumerals.parse("X"),10);
        }

        @Test(expected = Exception.class)
        public void parseNoCharacterStringAndThrowExceptionTest() throws Exception {
            assertEquals(romanNumerals.parse(""),1);
        }

        @Test(expected = Exception.class)
        public void parseIncorrectRomanNumberAndThrowExceptionTest() throws Exception {
            assertEquals(romanNumerals.parse("15"),10);
        }
    }
