package org.example;

public class RomanNumerals {

    int parse(String number) throws Exception {
        if (number.equals("I")) {
            return 1;
        }
        if (number.equals("II")) {
            return 2;
        }
        if (number.equals("III")) {
            return 3;
        }
        if (number.equals("IV")) {
            return 4;
        }
        if (number.equals("V")) {
            return 5;
        }
        if (number.equals("VI")) {
            return 6;
        }
        if (number.equals("VII")) {
            return 7;
        }
        if (number.equals("VIII")) {
            return 8;
        }
        if (number.equals("IX")) {
            return 9;
        }
        if (number.equals("X")) {
            return 10;
        }
        throw new Exception("Insert correct type of number");
    }
}

